
new Vue({
  el: '#app',
  data: {
    obj:'ananas',
    datas:[
    ],
    pwd:'',
    login:'',
    action:'Add',
    id:'',
    bool:true,
  },
  mounted() {
    if (this.datas.length!=0) {
        this.bool=false
    }
  },
  methods: {
    Add(id){
        if (this.action==="Update") {
            this.Delete(this.id)
            this.datas.push({id:this.id,login:this.login,pwd:this.pwd}) 
        }
        else{
            this.datas.push({id:Math.random().toString(36).substring(2),login:this.login,pwd:this.pwd})  
        }
        this.pwd=''
        this.login=''
        this.id=''
        this.action="Add"
        if (this.datas.length!=0) {
            this.bool=false
        }
    },
    Delete(id){
        let tab=[]
        this.datas.forEach(elt => {
            if (elt.id!=id) {
                tab.push(elt)
            }
        });
        this.datas=[]
        this.datas=tab
        if (this.datas.length==0) {
            this.bool=true
        }
    },
    Update(data){
        this.action="Update"        
        this.pwd=data.pwd
        this.id=data.id
        this.login=data.login        
    }
  },
})